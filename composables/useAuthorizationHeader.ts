export const useAuthorizationHeader = (contentType: string) => {
    const tokenCookie = useCookie("aei-desktop-token");

    return {
        "Content-Type": contentType,
        "Authorization": "Bearer "+ tokenCookie.value
    };
}
