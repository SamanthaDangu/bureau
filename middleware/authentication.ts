import jwt_decode from "jwt-decode";

export default defineNuxtRouteMiddleware(() => {
    const { baseURL } = useRuntimeConfig().app;
    const tokenCookie = useCookie("aei-desktop-token");

    if (tokenCookie.value === undefined) {
        return navigateTo(baseURL + 'connexion');
    }
    const decodedToken = jwt_decode(tokenCookie.value);
    const userRoles = decodedToken.roles;

    if (!userRoles.includes("ROLE_ELECTED") && !userRoles.includes("ROLE_DIRECTOR")) {
        return navigateTo(baseURL + 'index-agent');
    }
});