const getEmployerUsers = async function (event: Event) {
    const { apiUrl } = useRuntimeConfig();
    const { employerId } = event.context.params;

    const employerUsers = await $fetch(apiUrl + 'employers/' + employerId + '/users', {
        method: 'GET',
        headers: {
            'content-type': 'application/json',
            'authorization': event.req.headers.authorization
        }
    })
        .catch((error) => {
            return error;
        });

    return employerUsers["hydra:member"];
}

export default defineEventHandler(getEmployerUsers);
