const getEmployerActiveAgents = async function (event: Event) {
  const { apiUrl } = useRuntimeConfig();
  const { employerId } = event.context.params;

  const employerActiveAgents = await $fetch(apiUrl + 'employers/' + employerId + '/active-agents', {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'authorization': event.req.headers.authorization
    }
  })
    .catch((error) => {
      return error;
    });

  return employerActiveAgents["hydra:member"];
}

export default defineEventHandler(getEmployerActiveAgents);
