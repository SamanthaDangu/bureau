import { useQueryURLToString } from '../../../../composables/useQueryURLToString';

const getEmployerInterventions = async function(event: Event) {
  const { apiUrl } = useRuntimeConfig();
  const { employerId } = event.context.params;
  const query = useQueryURLToString(event);

  const interventions = await $fetch(apiUrl + 'employers/' + employerId + '/interventions' + query, {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'authorization': event.req.headers.authorization
    }
  })
  .catch((error) => {
    return error;
  });

  return {
      interventions: interventions['hydra:member'],
      total: interventions['hydra:totalItems']
  };
}

export default defineEventHandler(getEmployerInterventions);
