const patchIntervention = async function(event) {
    const { apiUrl } = useRuntimeConfig();
    const { id } = event.context.params;
    const body = await readBody(event);

    return await $fetch(apiUrl + 'interventions/' + id, {
      method: 'PATCH',
      headers: {
        'content-type': 'application/merge-patch+json',
        'authorization': event.req.headers.authorization
      },
      body: body
    });
}

export default defineEventHandler(patchIntervention);
