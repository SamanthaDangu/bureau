const getPriorities = async function (event: Event) {
    const { apiUrl } = useRuntimeConfig();

    const priorities = await $fetch(apiUrl + 'priorities', {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
        'authorization': event.req.headers.authorization
      }
    })
    .catch((error) => {
      return error;
    });

  return priorities["hydra:member"];
}

export default defineEventHandler(getPriorities);
