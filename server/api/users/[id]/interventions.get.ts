import { useQueryURLToString } from '../../../../composables/useQueryURLToString';

const getAssignedNotFinishedInterventions = async function(event) {
  const { apiUrl } = useRuntimeConfig();
  const { id } = event.context.params;
  const query = useQueryURLToString(event);

  const interventions = await $fetch(apiUrl + 'users/' + id + '/interventions' + query, {
    method: 'GET',
    headers: {
      'content-type': 'application/json',
      'authorization': event.req.headers.authorization
    }
  })
  .catch((error) => {
    return error;
  });

  return {
      interventions: interventions['hydra:member'],
      total: interventions['hydra:totalItems']
  };
}

export default defineEventHandler(getAssignedNotFinishedInterventions);
