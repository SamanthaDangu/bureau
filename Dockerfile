FROM node:20 as build

WORKDIR /app
COPY package.json package-lock.json /app/
RUN npm ci --cache /.npm
COPY . .
RUN npm run build

FROM gcr.io/distroless/nodejs20-debian11

WORKDIR /app
COPY --from=build /app/.output /app/

EXPOSE 3000
ENV HOST=0.0.0.0
ENV PORT=3000
ENV NODE_ENV=production

CMD ["server/index.mjs"]
